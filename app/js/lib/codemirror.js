$(function() {
  'use strict';

  var $body = $('body');

  window.initCodemirror = function($text) {
    if ($text.is(':hidden')) { return; }
    var heightDiff = $text.offset().top + $('footer').height() + 20;
    var lang = $text.data('language');
    var instance = CodeMirror.fromTextArea($text[0], {
      indentUnit: 2,
      tabSize: 2,
      undoDepth: 3000,
      // readOnly: 'nocursor',
      autofocus: true,
      electricChars: true, // auto-indentation by content
      lineWrapping: true,
      lineNumbers: true,
      // keyMap: 'vim',
      // mode: 'text/x-csrc'
      // mode: 'ruby'
      mode: lang,
      matchBrackets: true,
      showCursorWhenSelecting: true,
      theme: 'material'
    });
    instance.setSize(null, $('html').innerHeight() - heightDiff);
    CodeMirror.commands.save = function(cm) {
      cm.save();
      $(cm.getTextArea()).parents('form').submit()
      $body.trigger('notify', {
        type: 'info',
        message: 'Saving project logic.'
      });
    };
    CodeMirror.Vim.map('jk', '<Esc>', 'insert');
    CodeMirror.Vim.map('qw', ':w');
    instance.setOption("extraKeys", {
      'Ctrl-X': function(cm) {
        $('#test-link').click();
      }
    });
    $text.removeClass('codemirror-pending').addClass('codemirror-enabled');
    $text.data('codemirror', instance);
  }

  $('body').on('click.codemirror-switch-keymap', '.codemirror-switch-keymap', function() {
    var $this = $(this);
    var $text = $($this.data('instance'));
    var instance = $text.data('codemirror');
    if (instance.getOption("keyMap") === 'default') {
      instance.setOption("keyMap", "vim");
      $this.text('VIM Mapping');
    } else {
      instance.setOption("keyMap", "default");
      $this.text('Default Mapping');
    }
  });

  $(window).on('statechangecomplete', function() {
    $('.codemirror-pending').each(function() {
      initCodemirror($(this));
    });
  });


});
