$(function() {

  var $window = $(window);
  var $picker = null;
  var $inputDay = null;
  var $inputMonth = null;
  var $inputYear = null;
  var monthNames = [null, "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  var multipick = false;
  var highlightUrl = null;
  var pickableUrl = null;
  var allowPass = false;

  // helpers
  function getDay() { return $inputDay.val(); }
  function getMonth() { return parseInt($inputMonth.val()); }
  function getYear() { return parseInt($inputYear.val()); }

  function setDay(day) {
    $inputDay.val(day).change();
  }

  function toggleDay(day) {
    day = day.toString();
    var d = getDay();
    d = d.toString().split(',');
    d = _.compact(d);
    if (d.indexOf(day) === -1) { // not exist
      d.push(day);
    } else {
      d = _.without(d, day);
    }
    d = _.uniq(d).join(',');
    setDay(d);
  }

  function setMonth(month) {
    $inputMonth.val(month).change();
  }

  function setYear(year) {
    $inputYear.val(year).change();
  }

  function updateMonthName() {
    var name = monthNames[getMonth()] + ' ' + getYear();
    $('div.month-switch span.month', $picker).text(name);
  }

  function updateFirstDayPadding() {
    var d = new Date();
    d.setYear(getYear());
    d.setMonth(getMonth() - 1);
    d.setDate(1);
    var nSpaces = d.getDay();
    $days.first().attr('pad', nSpaces);
  }

  function stepMonth(step) {
    var m = getMonth() + step;
    if (m === 0) {
      m = 12;
      setYear(getYear() - 1);
    }
    if (m === 13) {
      m = 1;
      setYear(getYear() + 1);
    }
    setDay('');
    setMonth(m);
    updateCalendar();
    updateAddon();
  }

  function toggleLastDays() {
    var d = new Date();
    d.setYear(getYear());
    d.setMonth(getMonth());
    d.setDate(0);
    var lastDay = d.getDate();
    $days.filter('.hidden').removeClass('hidden');
    _.range(lastDay+1, 32).forEach(function(i) {
      $days.filter('.day-'+i).addClass('hidden');
    });
  }

  function toggleCurrentDay() {
    $days.filter('.current').removeClass('current');
    getDay().split(',').forEach(function(d) {
      $days.filter('.day-'+d).addClass('current');
    });
  }

  function hidePrevBtnIfNoPass() {
    $('a.prev-month', $picker).show(0);
    if (!allowPass) {
      var d = new Date();
      if (getMonth() === d.getMonth() + 1 && getYear() === d.getFullYear()) {
        $('a.prev-month', $picker).hide(0);
      }
    }
  }

  // flows
  function initPlugin() {
    $picker = $('div.month-calendar-picker');
    if (!$picker.length) return;
    $throbber = $('div.loading-throbber', $picker);
    highlightUrl = $picker.data('fetch-highlights');
    pickableUrl = $picker.data('fetch-pickables');
    multipick = !!$picker.data('multipick');
    allowPass = !!$picker.data('allow-pass');
    $inputDay = $('input.day', $picker);
    $inputMonth = $('input.month', $picker);
    $inputYear = $('input.year', $picker);
    $days = $('div.days span.day', $picker);
    assignTodayIfNoDayGiven();
    updateCalendar();
    updateAddon();
    setTimeout(function() {
      $inputDay.change();
    }, 100);
  }

  function assignTodayIfNoDayGiven() {
    if (!getDay()) {
      var d = new Date();
      setMonth(d.getMonth() + 1);
      setYear(d.getFullYear());
    }
  }

  function updateCalendar() {
    updateMonthName();
    updateFirstDayPadding();
    toggleLastDays();
    toggleCurrentDay();
    hidePrevBtnIfNoPass();
  }

  function startThrobber() {
    var c = $throbber.data('count') || 0;
    $throbber.data('count', c + 1);
    $throbber.show(0);
  }

  function endThrobber() {
    var c = $throbber.data('count');
    $throbber.data('count', c - 1);
    if (c === 1) {
      $throbber.hide(0);
    }
  }

  function updateAddon() {
    if (highlightUrl) {
      $days.removeAttr('style').tooltip('destroy');
      startThrobber();
      $.get(highlightUrl, {month:getMonth(), year:getYear()}).done(function(days) {
        for (var d in days) {
          var msg = days[d].msg;
          var color = days[d].color;
          $days.filter('.day-'+d)
            .css('background-color', color)
            .tooltip({title: msg});
        }
        endThrobber();
      }).fail(function() {
        alert('No data is available');
        endThrobber();
      });
    }
    if (pickableUrl) {
      $days.removeClass('pickable');
      startThrobber();
      $.get(pickableUrl, {month:getMonth(), year:getYear()}).done(function(days) {
        days.forEach(function(d) {
          $days.filter('.day-'+d).addClass('pickable');
        });
        endThrobber();
      }).fail(function() {
        alert('No data is available');
        endThrobber();
      });
    } else { // all are pickable
      $days.addClass('pickable');
    }
  }

  // events
  $window.on('statechangecomplete', function() {
    initPlugin();
  });

  $('body').on('click', 'div.month-calendar-picker a.prev-month', function() {
    stepMonth(-1);
  });

  $('body').on('click', 'div.month-calendar-picker a.next-month', function() {
    stepMonth(1);
  });

  $('body').on('click', 'div.month-calendar-picker div.days span.day.pickable', function() {
    var d = $(this).data('day');
    if (multipick) {
      toggleDay(d);
    } else {
      setDay(d);
    }
    toggleCurrentDay();
  });

  $('body').on('click', 'div.month-calendar-picker [data-pick-day]', function() {
    if (!multipick) return false;
    var $this = $(this);
    var pickDay = $this.data('pick-day');
    var pad = $days.first().attr('pad');
    var mod = (pickDay - pad + 15) % 7;
    var days = _.filter(_.range(1,32), function(i) { return i % 7 == mod; });
    days.forEach(function(d) {
      toggleDay(d);
    });
    toggleCurrentDay();
  });

});
