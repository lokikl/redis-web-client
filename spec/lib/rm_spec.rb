require 'spec_helper'

# Usage:
if false
  User = RM[:user]
  Group = RM[:group]
  GroupOwnership = RM[:group_ownership]

  loki = User.create(name: 'loki', email: 'loki@lokinote.com', password: '123123')
  developer = Group.create(name: 'developer')

  ownership = GroupOwnership.create(user_uuid: loki, group_uuid: developer)
  GroupOwnership.get(Group.get(developer)['group_ownership_uuid'])['user_uuid']

  Group.link(uuid: developer, target_uuid: loki)
  User.all
  User.all(group: developer)
  User.all(name: 'loki')
  User.unlink(uuid: loki, target_uuid: developer)
  User.hook(:after_destroy) {
    puts "User removed"
  }
  User.destroy(uuid: loki)
end

describe RM do
  before(:each) do
    redis = Redis.new
    redis.keys("resource:*").each { |key| redis.del(key) }
    redis.keys("index:*").each { |key| redis.del(key) }

    @skills = RM[:skill]
    @categories = RM[:category]
    @groups = RM[:group]
    @group_ownerships = RM[:group_ownership]
    @users = RM[:user]
  end

  it :list do
    # puts RM.list.to_yaml
    expect(RM.list).to be_a(Hash)
  end

  it :create do
    expect(@skills.create(name: 'badminton')).to be_a(String)
  end

  it :get do
    uuid = @skills.create(name: 'badminton')
    expect(@skills.get(uuid)['name']).to be == 'badminton'
  end

  it :destroy do
    uuid = @skills.create(name: 'badminton')
    @skills.destroy(uuid: uuid)
    expect(@skills.all).to be_empty
  end

  it :update do
    skills = @skills
    uuid = skills.create(name: 'badminton')
    skills.update(uuid: uuid, name: 'tennis')
    expect(skills.get(uuid)['name']).to be == 'tennis'
  end

  describe :all do
    it 'filters' do
      3.times { |i|
        @users.create(email: "dummy#{i+1}@abc.com", name: 'dummy', password: 'dummy')
      }
      expect(@users.all(email: "dummy2@abc.com").length).to be == 1
      expect(@users.all(name: "dummy").length).to be == 3
    end
  end

  describe :link do
    it '1_to_n' do
      @categories.create(name: 'dummy')
      @skills.create(name: 'dummy')
      c_uuid = @categories.create(name: 'sport')
      s_uuid = @skills.create(name: 'badminton', category_uuid: c_uuid)
      expect(@skills.all.length).to be == 2

      # link a skill to a category
      expect(@skills.all(category: c_uuid).length).to be == 1
      expect(@skills.get(s_uuid)['category_uuid']).to be == c_uuid

      # unlink
      @categories.unlink(uuid: c_uuid, target_uuid: s_uuid)
      expect(@skills.all(category: c_uuid).length).to be == 0
      expect(@skills.get(s_uuid)['category_uuid']).to be_nil

      # link a category to a skill
      @skills.update(uuid: s_uuid, category_uuid: c_uuid)
      expect(@skills.all(category: c_uuid).length).to be == 1
      expect(@skills.get(s_uuid)['category_uuid']).to be == c_uuid

      # unlink
      @skills.unlink(uuid: s_uuid, target_uuid: c_uuid)
      expect(@skills.all(category: c_uuid).length).to be == 0
      expect(@skills.get(s_uuid)['category_uuid']).to be_nil

      # destroy skill
      @skills.destroy(uuid: s_uuid)
      expect(@skills.all(category: c_uuid).length).to be == 0

      # destroy category
      s_uuid = @skills.create(name: 'badminton')
      @skills.link(uuid: s_uuid, target_uuid: c_uuid)
      expect(@skills.get(s_uuid)['category_uuid']).to be == c_uuid
      @categories.destroy(uuid: c_uuid)
      expect(@skills.get(s_uuid)['category_uuid']).to be_nil
    end

    it :n_to_n do
      @groups.create(name: 'dummy')
      @users.create(email: 'dummy', name: 'dummy', password: 'dummy')
      g_uuid = @groups.create(name: 'developers')
      u_uuid = @users.create(email: 'loki@lokinote.com', name: 'loki', password: '123123')

      # actually either one is good enough, no need to call twice here
      @groups.link(uuid: g_uuid, target_uuid: u_uuid)

      # get groups by a given user, or reverse
      expect(@groups.all(user: u_uuid).length).to be == 1
      expect(@users.all(group: g_uuid).length).to be == 1

      # unlink
      @groups.unlink(uuid: g_uuid, target_uuid: u_uuid)
      expect(@groups.all(user: u_uuid).length).to be == 0

      @groups.link(uuid: g_uuid, target_uuid: u_uuid)
      expect(@groups.all(user: u_uuid).length).to be == 1

      @groups.destroy(uuid: g_uuid)
      expect(@groups.all(user: u_uuid).length).to be == 0
    end

    it :with_external_resource do
      group = @groups.create(name: 'developer')
      user = @users.create(email: 'loki@lokinote.com', name: 'loki', password: '123123')
      ownership = @group_ownerships.create(group_uuid: group, user_uuid: user)
      expect(
        @group_ownerships.get(@groups.get(group)['group_ownership_uuid'])['user_uuid']
      ).to be == user
      expect(@group_ownerships.all(user: user).length).to be == 1

      group2 = @groups.create(name: 'singer')
      ownership = @group_ownerships.create(group_uuid: group2, user_uuid: user)
      expect(@group_ownerships.all(user: user).length).to be == 2
    end

    it :auto_update do
      music = @categories.create(name: 'music')
      sport = @categories.create(name: 'sport')
      tennis = @skills.create(name: 'tennis', category_uuid: music)
      expect(@skills.all(category: music).length).to be == 1
      expect(@skills.all(category: sport).length).to be == 0

      @skills.update(uuid: tennis, category_uuid: sport)
      expect(@skills.all(category: music).length).to be == 0
      expect(@skills.all(category: sport).length).to be == 1
    end
  end

  it :hook do
    a = 0
    @skills.hook(:before_save) { |current:, updates:|
      a += 1
      expect(current).to be_a(Hash)
      expect(updates).to be_a(Hash)
    }
    @skills.hook(:after_save) { |current:, updates:|
      a += 1
      expect(current).to be_a(Hash)
      expect(updates).to be_a(Hash)
    }
    @skills.hook(:before_destroy) { |model:|
      a += 1
      expect(model).to be_a(Hash)
    }
    @skills.hook(:after_destroy) { |model:|
      a += 1
      expect(model).to be_a(Hash)
    }
    skill = @skills.create(name: 'tennis')
    expect(a).to be == 2
    @skills.update(uuid: skill, name: 'badminton')
    expect(a).to be == 4
    @skills.destroy(uuid: skill)
    expect(a).to be == 6
  end
end
