class Marktime
  attr_reader :total

  def initialize
    @total = 0
  end

  def mark message, &block
    require 'benchmark'
    printf "%-50s", message
    time = Benchmark.realtime &block
    puts " | #{time.round(4)}s"
    @total += time
  end
end
