#encoding: utf-8
require 'sinatra/support'
require 'openssl'
require 'lib/rm'

class WebApp < AppBase
  helpers Sinatra::UserAgentHelpers

  # RM should always talk to localhost 6379 redis
  helpers do
    def redis_conn(profile_id)
      @profile = RM['profile'].get(profile_id)
      Redis.new(
        host: @profile['host'],
        port: @profile['port'],
        db: @profile['db'],
        password: @profile['password'] == '' ? nil : @profile['password'],
      )
    end
  end

  before do
    response.headers['Current-Url'] = request.url
  end

  not_found do
  end

  get '/?' do
    profiles = RM['profile'].all
    render_page(
      page: 'select-profile',
      title: 'RWC | Select Profile',
      locals: {
        profiles: profiles,
      },
    )
  end

  post '/create-profile/?' do
    hash = params['profile']
    RM['profile'].create(hash.symbolize_keys)
    redirect_back
  end

  post '/update-profile/:profile_id/?' do |profile_id|
    RM['profile'].update(params['profile'].merge('uuid' => profile_id).symbolize_keys)
    redirect_to '/'
  end

  post '/delete-profile/:profile_id/?' do |profile_id|
    RM['profile'].destroy(uuid: profile_id)
    redirect_to '/'
  end

  get '/edit-profile/:profile_id/?' do |profile_id|
    redis_conn(profile_id) # load @profile
    render_page(
      page: 'edit-profile',
      title: "RWC | Edit Profile #{profile_id}",
      locals: {
      },
    )
  end

  post '/:profile_id/update-value/?' do |profile_id|
    begin
      redis = redis_conn(profile_id) # load @profile
      case redis.type(params['key'])
      when 'string'
        redis.set(params['key'], params['new_value'])
      when 'set'
        redis.sadd(params['key'], params['new_value'])
      end
      redirect_back("Successfully updated [#{params['key']}]")
    rescue => ex
      redirect_err("Failed due to #{ex.message}")
    end
  end

  def process_keys(keys:, level:)
    keys.each_with_object({}) { |key, h|
      shortened = key.split(':').first(level).join(':')
      h[shortened] = key == shortened
    }
  end

  def list_keys(profile_id:)
    redis = redis_conn(profile_id)
    filter = params['f'].to_s + '*'
    all_keys = redis.keys(filter)

    filter_type = params['t']
    filter_type = 'all' if filter_type.to_s == ''
    all_keys.delete_if { |key|
      filter_type != redis.type(key)
    } if filter_type != 'all'

    max_count = 60
    max_parts = all_keys.max { |k| k.split(":").length }.split(":").length rescue 0
    filter_parts = filter.split(':').length

    back_filter = filter.split(':').first(filter_parts - 2).join(':') rescue ''
    back_filter += ':' if back_filter != ''

    keys = {}
    [*[filter_parts, 1].max..max_parts].each { |i|
      new_keys = process_keys(keys: all_keys, level: i)
      if new_keys.keys.length >= max_count
        keys = new_keys if keys.blank?
        break;
      end
      keys = new_keys
    }
    keys.each { |k, v| keys[k] = redis.type(k) if v }

    render_page(
      page: 'list',
      title: "RWC - #{@profile['name']} - #{filter_type} #{filter}",
      locals: {
        keys: keys,
        back_filter: back_filter,
        filter: params['f'],
        filter_type: params.fetch('t', 'all'),
        matches_count: all_keys.length,
      },
    )
  end

  get '/:profile_id/?' do |profile_id|
    if params['v']
      get_key(profile_id: profile_id)
    else
      list_keys(profile_id: profile_id)
    end
  end

  def get_key(profile_id:)
    key = params[:f]
    redis = redis_conn(profile_id)
    type = redis.type(key).to_sym
    if type == :none
      redirect_to "/#{profile_id}/?t=#{params[:t]}&f=#{key}"
    else
      value = case type
      when :string
        redis.get(key)
      when :list
        redis.lrange(key, 0, -1).each_with_index.to_a.map(&:reverse)
      when :set
        redis.smembers(key).each_with_index.to_a.map(&:reverse)
      when :zset
        redis.zrange(key, 0, -1, with_scores: true)
      when :hash
        redis.hgetall(key)
      end
      render_page(
        page: 'value',
        title: 'RWC',
        locals: {
          key: key,
          type: type,
          value: value,
        },
      )
    end
  end

  post '/:profile_id/delete/?' do |profile_id|
    redis = redis_conn(profile_id)
    query = Rack::Utils.parse_nested_query(URI.parse(request.referer).query)
    keys = redis.keys(query['f'].to_s + '*')
    filter_type = query['t']
    filter_type = 'all' if filter_type.to_s == ''
    if filter_type != 'all'
      keys.delete_if { |key| redis.type(key) != filter_type }
    end
    keys.each { |key| redis.del(key) }
    redirect_back
  end

  post '/:profile_id/delete-item/?' do |profile_id|
    redis = redis_conn(profile_id)
    case redis.type(params['key'])
    when 'set'
      redis.srem(params['key'], params['subkey'])
    end
    redirect_back
  end

end

# models
# require_relative 'indicator'
