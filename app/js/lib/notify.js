$(function initNotify() {

  var $body = $('body');

  $body.on('notify', function(e, payload) {
    var q = {
      delay: 2000, type: payload.type,
      placement: { from: "top", align: "center" },
      animate: { enter: 'animated fadeInDown', exit: 'animated fadeOutUp' }
    }
    if (payload.isLoading) {
      _.extend(q, {
        "allow_dismiss": false,
        "showProgressbar": true
      });
    }
    $.notify({ message: payload.message }, q);
  });

  $(window).on('statechangecomplete', function() {
    var $notify = $('[data-notify-type]');
    if ($notify.length > 0) {
      $body.trigger('notify', {
        type: $notify.data('notify-type'),
        message: $notify.html()
      });
      $notify.remove();
    }
  });

});
