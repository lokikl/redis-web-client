$(function() {

  function perform($obj, action) {
    var sel = $obj.data('toggle-sel');
    var klass = $obj.data('toggle-class') || 'show';
    $(sel)[action + 'Class'](klass);
  }

  $('body').on('click.toggle', '[data-toggle-sel]', function() {
    var $this = $(this);
    perform($this, 'toggle');
  });

  $(window).on('statechangecomplete', function() {
    $('[data-toggle-sel]').each(function() {
      var $this = $(this);
      var defaultOff = $this.data('toggle-default-off');
      if (defaultOff) {
        perform($this, 'remove');
      }
    });
  });

});
