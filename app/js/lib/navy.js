//
// navy
//   - no nothing when requesting url is identical to the current one
//   - default is replacing state
//   - navy-skip: traditional navigation
//   - navy-base: default target
//   - navy-land: add new state
//   - data-navy-aimed:
//     jq query, multiple allows, match occurrences one by one, default to body
//     since pesudo-element used in css, so please aim at nodes without before or after
//   - whenever error occurred, alert as this is a design issue
//
(function(window,undefined){
  $(function(){
    var $body = $('body');
    var htmlCache = {};

    function changeDocumentTitle(title) {
      if (title === '') { return; }
      window.document.title = title;
      try {
        var titleHTML = window.document.title
          .replace('<','&lt;')
          .replace('>','&gt;')
          .replace(' & ',' &amp; ');
        window.document.getElementsByTagName('title')[0].innerHTML = titleHTML;
      }
      catch ( Exception ) { }
    }

    // Internal Helper
    // $('a:internal') to select all internal links
    $.expr[':'].internal = function(obj, index, meta, stack){
      var url = $(obj)[0].href || '';
      var rootUrl = window.History.getRootUrl();
      return url.substring(0,rootUrl.length) === rootUrl;
    };


    function render(html, title, url, aimed) {
      $(aimed).html('<p>Rendering...</p>');
      // console.log("RENDERING");
      // console.log("title: " + title);
      // console.log("url: " + url);
      // console.log("aimed: " + aimed);
      var $page = $(html);
      changeDocumentTitle(title);

      var $newbody = $('<div></div>');
      $newbody.html($page.find('.navy-base'));
      var $originals = $(aimed);
      var allFound = true;
      $(aimed, $newbody).each(function(i) {
        var $node = $(this);
        var $target = $($originals[i]);
        if ($target.length > 0) {
          $target.replaceWith($node);
        } else {
          allFound = false;
          return false;
        }
      });
      if (!allFound) {
        console.log('not all target found, so render to navy base');
        render(html, title, url, '.navy-base');
      }
    }

    function goWithNavy(isPeek, aimed, action, url) {
      // original href will be treated as a backup plan (nojs set)
      $(aimed).addClass('navy-aimed');
      $.ajax({
        url: url,
        success: function(html, status, xhr){
          var $page = $(html);
          var title = $page.filter('title').text();
          if (isPeek) {
            render(html, title, url, aimed);
            $(window).trigger('statechangecomplete');
          } else {
            // get url again here for redirection
            var newUrl = xhr.getResponseHeader('Current-Url') || url;
            var timestamp = (new Date()).getTime();
            htmlCache[timestamp] = html;
            window.History[action]({
              title: title, url: url,
              aimed: aimed,
              ts: timestamp, // to ensure statechange will be triggered
            }, title, newUrl);
          }
        },
        error: function(xhr, textStatus, errorThrown){
          var errMsg = xhr.responseText;
          alert(url + '\n\n' + errMsg);
        }
      });
    }
    window.goWithNavy = goWithNavy;

    var target = 'a:internal:not(.navy-skip):not([target=_blank])';
    $body.on('click', target, function(event) {
      // Continue as normal for cmd clicks etc, open new tabs...
      if ( event.which == 2 || event.metaKey ) { return true; }
      var $node = $(this);
      var action = $node.hasClass('navy-land') ? 'pushState' : 'replaceState';
      var url = $node.attr('href');
      // var currentUrl = location.pathname + location.search;
      // if (url === currentUrl) return false;
      if (url[0] != '#') {
        event.preventDefault();
        var aimed = $node.data('navy-aimed') || '.navy-base';
        var isPeek = $node.hasClass('navy-peek');
        goWithNavy(isPeek, aimed, action, url);
        return false;
      }
    }); // end of the click listener

    $body.on('submit', 'form:not(.navy-skip)', function(event) {
      var $form = $(this);
      var parsley = $form.data('Parsley');
      if (parsley && parsley.isValid() === false) {
        return false;
      }
      var url = $form.prop('action');
      var aimed = $form.data('navy-aimed') || '.navy-base';
      $(aimed).addClass('navy-aimed');
      $form.ajaxSubmit({
        url: url,
        success: function(html, status, xhr) {
          var newUrl = xhr.getResponseHeader('Current-Url') || url;
          var $page = $(html);
          var title = $page.filter('title').text();
          if ($form.hasClass('navy-peek')) {
            render(html, title, url, aimed);
            $(window).trigger('statechangecomplete');
            $("body").trigger('statechanged');
          } else {
            var timestamp = (new Date()).getTime();
            htmlCache[timestamp] = html;
            window.History.replaceState({
              title: title, aimed: aimed,
              ts: timestamp, // to ensure statechange will be triggered
            }, title, newUrl);
          }
        },
        error: function(xhr) {
          var errMsg = xhr.responseText;
          alert(url + '\n\n' + errMsg);
        }
      });
      return false;
    }); // end of the form submit listener

    $(window).bind('statechange',function(e) {
      var s = History.getState();
      // console.log(s);
      if (s) {
        var d = s.data;
        if (d && d.title) {
          render(htmlCache[d.ts], d.title, d.url, d.aimed);
          $(window).trigger('statechangecomplete');
        } else {
          window.location.reload();
        }
      }
    });

    $body.on('click', '[data-confirm]', function() {
      var $this = $(this);
      var text = $(this).data('confirm');
      if (!confirm(text)) return false;
    });

  }); // end onDomLoad
})(window);

