source "https://rubygems.org"

gem 'rake'
gem 'log4r'                                              # event logging

# http user agent (browser) string parsing
# fetching browser type, version, platform, is mobile?
# can compare to supported browsers
gem 'useragent'

gem 'sinatra'
gem 'sinatra_more'
gem 'sinatra-subdomain'
gem 'sinatra-contrib'
gem 'sinatra-support' # user agent helper, support browser.body_class & browser.iphone?
gem 'rack'
gem 'rack-contrib'
gem 'yajl-ruby', :require => 'yajl'                      # JSON parser/loader
gem 'pony'                                               # used for sending out email
gem 'excon'     # for frontend to talk to birdmin
gem 'time-lord'
gem 'redis'

# background worker
gem 'sidekiq'
gem 'sidekiq-status'
gem 'slim' # used in sidekiq console

# previously, thin is installed via sys pack 'thin' on production
gem 'thin'

gem 'eventmachine', :git => 'git://github.com/eventmachine/eventmachine.git'
gem 'nokogiri'

gem 'uglifier' # realtime uglify server generated javascript
gem 'pry' # since this is a developer platform, sometime we need rake p on production
gem 'pry-remote'

group :production do
  gem "therubyracer"
end

# gem install ruby_core_source linecache19 ruby-debug-base19 ruby-debug19 ruby-debug-ide19 -- --with-ruby-include=$rvm_path/src/$(rvm tools strings)
group :development do
  # irb tools
  gem "sprockets"
  gem 'yui-compressor'
  gem "sass"
  gem "sprockets-sass"
  gem 'pry-clipboard'
  gem 'pry-stack_explorer'
  gem 'pry-nav'
  gem 'capistrano'
  gem 'capistrano-ext'
  gem 'capistrano-sidekiq'
  gem 'rvm-capistrano'
  gem 'sprite-factory'
  gem 'foreman'
end

group :test do
  gem 'libnotify'
  gem 'ffi'
  gem 'rb-inotify'
  gem 'rspec'
end
