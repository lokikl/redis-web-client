$(function() {

  var $body = $('body');

  // ajax submit a form by jquery.form
  $body.on('click', '.ajax-submit', function() {
    var $this = $(this);
    var $form = $this.parents('form');

    var url = $this.data('override-action');
    $('.codemirror-enabled', $form).each(function() {
      var $editor = $(this);
      $editor.data('codemirror').save()
    });

    $form.ajaxSubmit({
      "url": url,
      "success": function(r) {
        if (r.message) {
          $body.trigger('notify', {type: 'info', message: r.message});
        }
      }
    });
  });

});
