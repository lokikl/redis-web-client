$(function() {

  // data-remote="/backtest-details/<%= test[:uuid] %>"
  // data-remote-target="#backtest-details"
  $('body').on('click', '[data-remote]', function() {
    var $this = $(this);
    var url = $this.data('remote');
    var $target = $($this.data('remote-target'));
    if ($target.length > 0 && url) {
      $target.html("<p>Loading...</p>");
      $.get(url, function(html) {
        $target.html(html);
      });
    }
  });

});
