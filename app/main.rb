
require 'sinatra/base'
require 'sinatra/contrib'

Dir["app/lib/*.rb"].each { |f| require f }
require 'app/base'      # load the base app
require 'app/helpers'   # load global helpers

require "app/controllers/main.rb"
