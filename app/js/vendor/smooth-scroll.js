// required gsap and it's scroll to plugin
//
$(function() {
  var $window = $(window);
  var $body = $('body');

  if (!$body.hasClass('is-mobile')) {
    // todo: make this two variables configurable via body data
    var scrollTime = 0.2;
    var scrollDistance = 53 * 1.8; // 53 is the default value on ubuntu 12.04

    // this smooth scroll needs ScrollTo plugin
    // http://bassta.bg/2013/05/smooth-page-scrolling-with-tweenmax
    $window.on("mousewheel DOMMouseScroll", function(event){

      event.preventDefault();

      var delta = event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3;
      var scrollTop = $window.scrollTop();
      var finalScroll = scrollTop - parseInt(delta*scrollDistance);

      TweenMax.to($window, scrollTime, {
        scrollTo : { y: finalScroll, autoKill:true },
        ease: Power1.easeOut,
        overwrite: 5
      });
    });
  }
});
