#!/usr/bin/env ruby

require 'pry-remote'

Encoding.default_external = "UTF-8"

require 'fileutils'
FileUtils.mkdir_p 'log'

# add all ruby scripts to $LOAD_PATH
lib_path = File.expand_path(File.dirname(__FILE__) + '/..')
$: << lib_path unless $:.include? lib_path

# require tool to measure load time
require 'app/lib/marktime'
timer = Marktime.new

timer.mark "Load basic gems and global variables" do
  ENV["RACK_ENV"] ||= "development"
  $production = ENV["RACK_ENV"] == 'production'
  $load_app ||= false

  require "bundler/setup"
  require 'singleton'
  require 'yajl'
  require 'json'
  require 'yaml'
end

if $load_app
  require 'app/main'
  Log.inf "Started at #{Time.now} in #{timer.total.round(4)}s"
  puts "Started in #{timer.total.round(4)}s"
end
