$(function() {

  $('body').on('click', '.tabtoggle a[data-switchclass]', function() {
    var $this = $(this);
    //if ($this.parents('li:first').hasClass('active')) return;

    var klass = $this.data('switchclass');
    if (!klass) return;
    var $menu = $this.parents('.tabtoggle:first');
    var $pages = $($menu.data('toggle'));
    $pages.stop(true, true).hide(0);
    $pages.filter("."+klass).stop(true, true).fadeIn(500);
    $menu.find('li.active').removeClass('active');
    $this.parents('li:first').addClass('active');
  });

  $(window).on('statechangecomplete', function() {
    $('.tabtoggle').each(function() {
      var $this = $(this);
      $this.find('a[data-switchclass]:first').click();
    });
  });

});
