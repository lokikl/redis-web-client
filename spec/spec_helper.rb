require 'rack/test'
require 'pry'
require 'pry-remote'

RSpec.configure do |config|
  config.include Rack::Test::Methods
  # do not run those that hasn't been fixed yet OR being disabled
  config.filter_run_excluding :broken => true
  config.filter_run_excluding :disabled => true

  config.color = true
  config.tty = true
end

# auto load related lib
parts = RSpec.configuration.files_to_run[0].split('/')
if parts[-2] == 'lib'
  klass = parts[-1].split('_spec')[0]
  if File.exists?("./app/lib/#{klass}.rb")
    require "./app/lib/#{klass}"
  elsif File.exists?("./lib/#{klass}.rb")
    require "./lib/#{klass}"
  end
end
