/* globals Faye */

$(function() {
  'use strict';

  var $body = $('body');
  var $siteWrap = $('#site-wrap');

  // faye connection
  var server = $body.data('faye-server');
  if (typeof Faye !== 'undefined' && server) {
    var client = new Faye.Client(server);
    // Turn this on to inspect faye log in console
    // Faye.logger = window.console;
    var channel = $(".job-state").data("faye-channel");
    var subscription = client.subscribe('/' + channel.replace(".", '-'), function(message) {
      if (message.type === 'job-update') {
        // create the bar if not exists yet
        if ($(".job-state .job-" + message.uuid).length === 0) {
          var $bar = $('.job-bar.template').clone();
          $bar.addClass('job-' + message.uuid).removeClass('template');
          $(".job-state").prepend($bar);
          $(".job-" + message.uuid).fadeIn(500);
        }

        // update the progress
        var $bar = $(".job-state .job-" + message.uuid + ' .progress-bar');
        var updateBar = function(progress, msg) {
          if (!msg) { msg = progress + "% Completed"; }
          $bar.css('width', progress + '%')
            .attr('aria-valuenow', progress)
            .html(msg);
          if (progress >= 100) {
            $bar.parent().parent().children('img').css({
              'visibility': 'hidden',
              'margin-left': '-15px'
            });
          }
        }

        if (message.failed) {
          updateBar(100, message.failed);
          $bar.removeClass('progress-bar-success').addClass('progress-bar-danger');
        }
        if (message.percent) { updateBar(message.percent); }
        // update the description
        if (message.description) {
          $(".job-state .job-" + message.uuid + " span.job-name").text(message.description);
        }
        // auto dismiss
        if (message.auto_dismiss && message.percent === 100) {
          $(".job-" + message.uuid).fadeOut(500, function() { $(this).remove(); });
        }
        // update the link
        if (message.link) {
          var linkHTML = '<a href="' + message.link + '"><i class="pull-right fa fa-info-circle fa-lg" style="color:green;margin-right:12px"></i></a>';
          $(".job-state .job-" + message.uuid + " span.link").html(linkHTML);
        }
        // amend padding bottom in order to scroll
        $siteWrap.css('margin-bottom', $('.job-state').height());
      }
    });

    $('body').on('click', '.dismiss-bar', function() {
      $(this).parents('.job-bar').addClass('dismiss').fadeOut(500, function() {
        // amend padding bottom in order to scroll
        $siteWrap.css('margin-bottom', $('.job-state').height());
      });
    });

    $(window).on('statechangecomplete', function() {
      $('[data-graph-channel]').each(function() {
        var $container = $(this);
        var subscription = client.subscribe($container.data('graph-channel'), function(message) {
          if (message.type === 'replace_html') {
            $container.html(message.html);
          }
        });
      });
    });
  }

});

