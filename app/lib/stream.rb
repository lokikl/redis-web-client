require 'excon'
require 'tilt'

class Stream
  def initialize(options)
    @channel = options.fetch(:channel)
    @templates = {}
  end

  def replace_html(options)
    @templates[options[:template]] ||= Tilt.new("app/views/pokerface/shared/#{options[:template]}.erb", trim: true)
    html = @templates[options[:template]].render(options)
    publish(options.merge(
      html: html,
      type: :replace_html,
    ))
  end

  def report_status(options)
    replace_html(options.merge(template: :progress))
  end

  def render_graph(title: nil, series: [], bands: [])
    yaxis = []
    series.each { |s|
      unless s.has_key?(:yAxis)
        yaxis << [s[:name].split('[')[0], s.delete(:support_line), s.delete(:height)]
      end
    }
    yaxis.uniq!(&:first)
    margin = 21 # px
    total_height = yaxis.map(&:last).inject(:+) + margin * (yaxis.length - 1) # px
    rmargin = margin * 100.0 / total_height # in %
    next_top = 0
    yaxis = yaxis.each_with_index.map { |(name, support_line, height), i|
      rheight = height * 100.0 / total_height # in %
      o = {
        offset: 0,
        # labels: { align: 'left', x: 3, y: 4 },
        title: { text: name },
        top: next_top.to_s + '%',
        height: rheight.to_s + '%',
        gridLineDashStyle: 'Dot',
        gridLineColor: '#DDDDDD',
        opposite: false,
        tickAmount: 10, # show 10 area
        minPadding: 0.01,
        maxPadding: 0.01,
        lineWidth: 1,
      }
      o[:linkedTo] = 0 if name.start_with?('K ') && i > 0
      o[:plotLines] = support_line.map { |i|
        { value: i.to_f, width: 1, color: 'steelblue', dashStyle: 'LongDash', zIndex: 2 }
      } if support_line
      next_top += rheight + rmargin
      o
    }
    series_data = series.map { |s|
      i = yaxis.index { |y| y[:title][:text] == s[:name].split('[')[0] }
      {yAxis: i}.merge(s)
    }

    get_time = ->(d) { d.is_a?(Array) ? d[0] : d[:x] }
    begin
      diff = get_time[series[0][:data][1]] - get_time[series[0][:data][1]]
      names = series.map { |s| s.fetch(:name) }

      firsttwo = series_data[0][:data].first(2).map { |d| get_time[d] }
      seconds = firsttwo[1] - firsttwo[0]
    rescue => ex
      require 'pry'; binding.pry
    end
    tickInterval = seconds / 60 / 1000 # 1 tick = ? minutes
    range = 2.5 * 24 * 3600 * 1000 # show 3 days

    replace_html(
      channel: @channel,
      template: :graph,
      graph_title: title,
      graph_options: {
        # title: {
        #   text: title || "Graph #{@channel.split('-').first(2).join('-')}",
        #   align: 'left',
        # },
        scrollbar: { enabled: false },
        navigator: { enabled: false },
        # navigator: { enabled: true, top: 40, height: 40 },
        legend: { enabled: false },
        # the thumbnail serie
        credits: { enabled: false },
        # zoom type is which axis to zoom when dragging the mouse
        chart: {
          height: total_height,
          zoomType: 'x',
          ignoreHiddenSeries: true,
        },
        plotOptions: {
          candlestick: {
            upColor: 'white',
            upLineColor: 'green',
            color: 'white',
            lineColor: 'red',
          },
          series: {
            lineWidth: 1,
            states: {
              hover: { lineWidth: 1, halo: { size: 0 }, },
            },
            dataGrouping: {
              enabled: false,
            },
            dataLabels: {
              borderRadius: 2,
              borderWidth: 1,
              borderColor: '#AAA',
              y: -6,
            },
          },
        },
        tooltip: {
          animation: false,
          borderColor: 'gray',
          borderWidth: 1,
          valueDecimals: 3,
          # followPointer: true,
          borderRadius: 2,
          crosshairs: { dashStyle: 'dot', color: '#111' },
          shadow: false,
          stickyTracking: true,
        },
        rangeSelector: { enabled: false },
        yAxis: yaxis,
        series: series_data,
        xAxis: {
          range: range,
          plotBands: bands,
        },
      }
    )
  end

  # render graph v2 for v2 project
  def render_graph_v2(title: nil, series: [], bands: [], yaxis: [])
    replace_html(
      channel: @channel,
      template: :graph,
      graph_title: title,
      graph_options: {
        # title: {
        #   text: title || "Graph #{@channel.split('-').first(2).join('-')}",
        #   align: 'left',
        # },
        scrollbar: { enabled: false },
        navigator: { enabled: false },
        # navigator: { enabled: true, top: 40, height: 40 },
        legend: { enabled: false },
        # the thumbnail serie
        credits: { enabled: false },
        # zoom type is which axis to zoom when dragging the mouse
        chart: {
          height: yaxis.map { |y| y[:height] }.inject(:+),
          zoomType: 'x',
          ignoreHiddenSeries: true,
        },
        plotOptions: {
          candlestick: {
            upColor: 'white',
            upLineColor: 'green',
            color: 'white',
            lineColor: 'red',
          },
          series: {
            lineWidth: 1,
            states: {
              hover: { lineWidth: 1, halo: { size: 0 }, },
            },
            dataGrouping: {
              enabled: false,
            },
            dataLabels: {
              borderRadius: 2,
              borderWidth: 1,
              borderColor: '#AAA',
              y: -6,
            },
          },
        },
        tooltip: {
          animation: true,
          borderColor: 'gray',
          borderWidth: 1,
          valueDecimals: 3,
          # followPointer: true,
          borderRadius: 2,
          crosshairs: { dashStyle: 'dot', color: '#111' },
          shadow: false,
          stickyTracking: true,
        },
        rangeSelector: { enabled: false },
        yAxis: yaxis,
        series: series,
        xAxis: {
          range: 2.5 * 24 * 3600 * 1000, # show 3 days
          plotBands: bands,
        },
      }
    )
  end

  # return true if everything's fine
  def self.test
    path = stream_conn.params[:path] + '/client.js'
    if stream_conn.get(path: path).status != 200
      raise "Unable to download client.js"
    end
  end

  private

  def publish(options)
    self.class.stream_conn.post(
      read_timeout: 120,
      body: 'message=' + { channel: @channel, data: options }.to_json,
      expects: [200, 201, 202],
      idempotent: true,
    )
  end

  def self.stream_conn
    Excon.new(Config.get(key: 'main.faye-server'), {
      write_timeout: 1,
      read_timeout: 1,
      connect_timeout: 1,
    })
  end
end
