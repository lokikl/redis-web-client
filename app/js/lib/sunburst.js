'use strict';

$(function() {

  var sel = '#sunburst';
  var selectedNodeUUID = null;
  var normalOpacity = 0.2;

  $(window).resize(function() {
    var $sunburst = $(sel);
    if ($sunburst.length === 0) return;
    $sunburst.html('');
    reloadSunburst();
  });

  $(window).on('statechangecomplete', reloadSunburst);

  function reloadSunburst() {
    var $sunburst = $(sel);
    if ($sunburst.length === 0) return;
    if ($sunburst.children().length > 0) return;
    var root = $sunburst.data('data');

    var width = $sunburst.innerWidth();
    // alert(width);
    var height = $('html').innerHeight() * 0.85;
    // var height = 700;
    var radius = Math.min(width, height) / 2;
    var color = d3.scale.category20c();

    var svg = d3.select(sel)
      .append("svg")
      .attr("width", width)
      .attr("height", height + 100);

    svg = svg
      .append("g")
      .attr("transform", "translate(" + width * 0.48 + "," + height * .52 + ")");

    var partition = d3.layout.partition()
      .sort(null)
      .size([2 * Math.PI, radius * radius])
      .value(function(d) { return 1; });

    var arc = d3.svg.arc()
      .startAngle(function(d) { return d.x; })
      .endAngle(function(d) { return d.x + d.dx; })
      .innerRadius(function(d) { return Math.sqrt(d.y); })
      .outerRadius(function(d) { return Math.sqrt(d.y + d.dy); });

    var path = svg.datum(root).selectAll("path")
      .data(partition.nodes)
      .enter().append("g")

    path.append("path")
      .attr("d", arc)
      .style("stroke", "#fff")
      .style("stroke-width", "1px")
      .style("cursor", "pointer")
      .style("fill", function(d) {
        var n = (d.from_way ? d.from_way : 'none') + ' ' + d.uuid;
        return color(n);
      })
      .classed('filter', true)
      .style("opacity", normalOpacity)
      .on("click", onClick)
      .on("mouseenter", onMouseEnter)
      .on("mouseleave", onMouseLeave)
      .each(function(d, i) {
        // http://www.visualcinnamon.com/2015/09/placing-text-on-arcs.html
        var arcd = d3.select(this).attr('d');
        if (arcd.indexOf('L') === -1) { return; }

        var newArc = arcd.split('L')[0];
        newArc = newArc.replace(/,/g , " ");

        if (isInBottom(d)) {
          var startLoc 	= /M(.*?)A/;
          var middleLoc = /A(.*?)0 [01] 1/;
          var endLoc 		= /0 ([01]) 1 (.*?)$/;

          var endParts = endLoc.exec( newArc );
          var newEnd = startLoc.exec( newArc )[1];
          var middleSec = middleLoc.exec( newArc )[1];
          newArc = "M" + endParts[2] + "A" + middleSec + "0 " + endParts[1] + " 0 " + newEnd;
        }//if

        //Create a new invisible arc that the text can flow along
        svg.append("path")
          .attr("id", "donutArc"+i)
          .attr("d", newArc)
          .style("fill", "none");
      });

    path.append("text")
      // .attr("x", 5)
      .attr("dy", function(d, i) { return isInBottom(d) ? -8 : 18; })
      .append("textPath")
        .attr("startOffset", "50%")
        .style("text-anchor","middle")
        // .attr("startOffset", "100%")
        // .style("text-anchor","end")
        .style("fill", function(d) {
          return d.from_way == 'yes' ? 'green' : 'black';
        })
        .attr("xlink:href", function(d, i) { return "#donutArc" + i; })
        .text(function(d) { return d.name});


    function changeOpacity(uuid, opacity) {
      d3.selectAll("path")
        .filter(function(n) { return n.uuid === uuid; })
        .style("opacity", opacity);
    }

    function onClick(node) {
      if (node.uuid === selectedNodeUUID) {
        // selectedNodeUUID = '';
      } else {
        changeOpacity(selectedNodeUUID, normalOpacity);
        selectedNodeUUID = node.uuid;
        var d = $('#sunburst').data('hid-nothing');
        window.goWithNavy(false, '#output, .toggle-btns', 'pushState', '?d=' + d + '&s=' + selectedNodeUUID);
      }
    }

    function onMouseEnter(node) {
      changeOpacity(node.uuid, 0.8);
    }

    function onMouseLeave(node) {
      if (node.uuid === selectedNodeUUID) return;
      changeOpacity(node.uuid, normalOpacity);
    }

    // preselected by querystring
    if ($sunburst.data('selected')) {
      selectedNodeUUID = $sunburst.data('selected');
      changeOpacity(selectedNodeUUID, 0.8);
    } else {
      selectedNodeUUID = null;
    }

    // return if the arc is in bottom of the pie
    function isInBottom(d) {
      var angle = (arc.startAngle()(d) + arc.endAngle()(d)) / 2;
      var deg = angle * 180 / Math.PI;
      return deg > 90 && deg < 270;
    }
    // function isInBottom(d) {
    //   // get center angle in degree
    //   var start = arc.startAngle()(d);
    //   var middle = (arc.startAngle()(d) + arc.endAngle()(d)) / 2;
    //   var end = arc.endAngle()(d);
    //
    //   return [start, middle, end].filter(function(angle) {
    //     var deg = angle * 180 / Math.PI;
    //     return deg > 90 && deg < 270;
    //   }).length > 1;
    // }

    d3.select(self.frameElement).style("height", height + "px");

  } // reload sunburst
}); // on document loaded
