$(function() {

  $('body').on('click.toggle', '[data-show-tab]', function() {
    var $this = $(this);
    var klass = $this.data('show-tab');
    $this.parents('.switch').children('li.active').removeClass('active');
    $this.parent().addClass('active');
    $('.'+klass).each(function() {
      var $this = $(this);
      var mainClass = $this.prop('class').split(' ')[0];
      $this.parent().find('.'+mainClass).hide(0);
      $this.stop(true, true).fadeIn(500);
    });
  });

  $(window).on('statechangecomplete', function() {
    $('[data-show-tab]:first').click();
  });

});
