//= require vendor/sha
//= require vendor/jquery-1.11.2
//= require vendor/bootstrap.3.3.4
//= require vendor/underscore-min
//= require vendor/fastclick
//= require vendor/parsley-2.0.0
//= require vendor/jquery.apprise.1.5.min
//= require vendor/hideshow
//= require vendor/jquery.tablesorter

//= require vendor/history
//= require vendor/history.html4
//= require vendor/history.adapter.jquery
//= require vendor/jquery.form
//= require ./lib/ajax-submit
//= require ./lib/navy

//= require_self

$(function() {
  'use strict';

  var $body = $('body');

  function initPage() {
    $('.fade-out').delay(2000).fadeOut();
  }

  $(window).on('statechangecomplete', function() {
    initPage();
    FastClick.attach(document.body);
  });

  $body.on('keyup.event', '.trigger-submit-when-keydown', function() {
    $(this).parents('form').submit();
  });

  $body.on('change.event', '.trigger-submit-when-change', function() {
    $(this).parents('form').submit();
  });

  $(window).trigger('statechangecomplete');
  $('[autofocus]').focus();

  function getSelectionText() {
    var text = "";
    if (window.getSelection) {
      text = window.getSelection().toString();
    } else if (document.selection && document.selection.type != "Control") {
      text = document.selection.createRange().text;
    }
    return text;
  }

  $(document).bind("mouseup", function(event) {
    if (event.button === 0 && $(event.target).hasClass('searchable')) {
      setTimeout(function() {
        var selected = getSelectionText();
        if (selected !== '') {
          selected = selected.replace(/^\s+|\s+$/g, '');
          event.preventDefault();
          $(".custom-menu").html('');

          var baseUrl = '/' + location.pathname.split('/')[1];
          var $a = $('<a href="' + baseUrl + '/?t=all&f=*'+selected+'">');
          $a.html('<i class="fa fa-search"></i> Search for: *' + selected + '*');
          $(".custom-menu").append($('<li>').append($a));
          $(".custom-menu").finish().toggle(100).css({
            top: (event.pageY - 50) + "px",
            left: (event.pageX) + "px"
          });
        }
      }, 10);
    }
  });

  $(document).bind("mousedown", function (e) {
    if (!$(e.target).parents(".custom-menu").length > 0) {
      $(".custom-menu").hide(100);
    }
  });
});

