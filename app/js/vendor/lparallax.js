// features
//   responsive, auto resize the viewport
//   fast, requestAnimationFrame
//
// usage
//   $('div.images').lparallax();
//
// requirement
//   make background attachment fixed, 50% positioned
(function($){
  var doFrame = (
    window.requestAnimationFrame       ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame    ||
    window.oRequestAnimationFrame      ||
    window.msRequestAnimationFrame     ||
    function(callback) {
      setTimeout(callback, 1000 / 60);
    }
  );
	var $window = $(window);
  var screenHeight = $window.height();

	$.fn.lparallax = function() {
    var images = this;
    var speedFactor = 0.5;
    var running = false;
    var viewportHeight = images.height();

    function resizeImage() {
      resizeViewport();
      //resizeBG();
      update();
    }

    function resizeViewport() {
      screenHeight = $window.height();
      viewportHeight = screenHeight * 0.5;
      images.height(viewportHeight);
    }

    //function resizeBG() {
    //  var minImgHeight = viewportHeight + (screenHeight - viewportHeight) * speedFactor;
    //  images.each(function() {
    //    var $this = $(this);
    //    if ($this.data('height') < minImgHeight) {
    //      $this.css('backgroundSize', 'auto ' + minImgHeight + 'px');
    //    } else {
    //      $this.css('backgroundSize', '');
    //    }
    //  });
    //}
		
		function update() {
      var pos = $window.scrollTop();				
      images.each(function() {
        var $this = $(this);
        var top = $this.offset().top; // this is changable since expandable element may change the position

        // Check if totally above or totally below viewport
        if (top + viewportHeight >= pos && top <= pos + screenHeight) {
          var posY = Math.round((top - pos) * speedFactor) + "px";
          $this.css('backgroundPositionY', posY);
          $this.removeClass('invisible');
        } else {
          $this.addClass('invisible');
        }
      });
		}		

    function bindEvents() {
      $window.on('resize', function() { doFrame(resizeImage); });
      $window.on('scroll', function() {
        if (!running) {
          running = true;
          doFrame(update);
          running = false;
        }
      });
    }

    bindEvents();
    resizeImage();
    //fetchNaturalSize(images, resizeImage);
	};
})(jQuery);
