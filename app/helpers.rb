#encoding: utf-8

require 'cgi'

class AppBase < Sinatra::Base

  helpers do
    def h text
      Rack::Utils.escape_html(text)
    end

    # pretty print json
    def pj json
      json = Yajl.load json if json.is_a? String
      html = JSON.pretty_generate json
      h "<pre>#{h html}</pre>"
    end

    def handle_maintenance_mode
      excluded = [
        "/api/cache/clear",
        "/maintenance",
        "/api/deploy",
        "/api/retrieve_cache",
      ].include? request.path
      if !excluded && @backend.conf("maintenance") == "true"
        session["maintenance"] = true if params["d"] == "1"
        unless session["maintenance"]
          redirect_to "/maintenance"
        end
      end
    end

    def request_ip
      request.env["HTTP_X_REAL_IP"] || request.env["REMOTE_ADDR"]
    end

    def css name
      %Q{<link rel="stylesheet" href="/assets/#{name}.css">}
    end

    def js name
      if $production
        %Q{<script async src="/assets/#{name}.js"></script>}
      else
        %Q{<script src="/assets/#{name}.js"></script>}
      end
    end

    def get_locale
      # Pulls the browser's language
      @env["HTTP_ACCEPT_LANGUAGE"][0,2]
    end

    def t(*args)
      # Just a simple alias
      I18n.t(*args)
    end

    def old_ie?
      ie = request.user_agent[/MSIE (\d)/, 1]
      ie &&= ie.to_i
      ie && ie < 9
    end

    def ie?
      !request.user_agent[/MSIE/].nil?
    end

    def halt_json obj
      content_type :json
      json = Yajl::Encoder.new.encode obj
      #Log.inf json
      halt json
    end

    # write base64 encoded(js reader API) image to file
    def write_base64_img filename, src
      File.open(filename, 'w') { |f|
        f.write Base64.decode64(src.sub(/^.*,/, ''))
      }
    end

    def request_json
      @request_json ||= Yajl.load @request_body
    end

    def redirect_back msg=nil
      session[:notice] = msg if msg
      redirect back
    end

    def redirect_err msg="Internal Server Error"
      session[:error] = msg
      redirect back
    end

    def redirect_to url, msg=nil, msglevel=:notice
      session[msglevel] = msg if msg
      redirect url
    end

    def redirect_to_err url, msg="Internal Server Error"
      session[:error] = msg
      redirect url
    end

    # used with sinatra-page.js
    def erb_out layout, page, title=nil, locals={}
      response.headers["Content-Type"] = "text/html"
      project = self.class.name.downcase
      erb :"#{project}/#{layout}/#{page}", :layout => :"#{project}/#{layout}", :locals => {
        page_title: title || "Untitled page #{page}", layout: layout,
      }.merge(locals)
    end

    def render_page(nolayout: false, layout: 'main', page:, title: nil, locals: {})
      response.headers["Content-Type"] = "text/html"
      erb(:"#{layout}/#{page}", {
        layout: nolayout ? false : layout.to_sym,
        locals: {
          page_title: title || "Untitled page #{page}",
          layout: layout,
        }.merge(locals)
      })
    end

    # place erb file location
    def erb_location
      file = @template_cache.instance_variable_get("@cache").keys[0][1]
      @_out_buf << %Q[<i class="erb-locator" style="display:none; visibility:hidden">#{file}</i>]
    end

    # construct x editable from database design, will get input type directly from datamapper
    def construct_xeditable opt
      field         = opt.fetch :field
      escape        = opt.fetch :escape, false # in case value contain html, escape it
      display_name  = opt.fetch :name, field.capitalize
      obj           = opt.fetch :obj
      mode          = opt.fetch :mode, "inline"
      disabled      = opt.fetch :disabled, false
      prop_type     = opt.fetch :prop_type, nil
      url_prefix    = opt.fetch :url_prefix, '/'
      placement     = opt.fetch :placement, 'top'
      obj_class     = obj.class
      prop          = obj_class.properties.detect { |p| p.name == field.to_sym }
      type          = obj_class.name.demodulize.underscore
      value         = obj.send(field).to_s
      display_value = opt[:display_value]
      display_value ||= value == "" ? "(Empty)" : value

      prop_type ||= prop.class < DataMapper::Property::Enum ?
        "enum" :
        prop.class.name.demodulize.downcase

      prop_type = prop_type.to_s

      input_type = case prop_type
        when "string" then "text"
        when "password" then "password"
        when "integer" then "number"
        when "text" then "textarea"
        when "shorttext" then "text"
        when "longtext" then "textarea"
        when "enum" then "select"
        when "boolean" then "select"
        when "select" then "select"
        when "date" then "date"
        when "checklist" then "checklist"
      end
      format = ''
      if prop_type == "select"
        source = "data-source='#{opt[:options].to_json}'"
      elsif prop_type == "checklist"
        source = "data-source='#{opt[:options].to_json}'"
        vs = Yajl.load(value).map(&:to_i) rescue []
        display_value = "#{vs.length} selected"
        value = "["
        value << vs.map { |v| "'#{v}'" }.join(',')
        value << "]"
      elsif prop_type == "enum"
        options = prop.options[:flags].map { |f| { value:f, text:f } }
        source = "data-source='#{options.to_json}'"
      elsif prop_type == "password"
        display_value = "********"
      elsif prop_type == "boolean"
        options = ["true", "false"].map { |f| { value:f, text:f } }
        source = "data-source='#{options.to_json}'"
      elsif prop_type == "date"
        format = 'data-format="dd/mm/yyyy" data-view-format="dd/mm/yyyy"'
      end

      disabled_str = disabled ? "data-disabled=true" : ""
      if escape
        display_value = "HTML Content"
        value = value.h
      end

      <<-EOS
      <a data-name="#{field}" class="xeditable" href="javascript:void(0)"
        data-type="#{input_type}" data-pk="#{obj.id}"
        data-url="#{url_prefix}#{type}/update_property"
        data-mode="#{mode}"
        data-value="#{value.to_s}"
        data-placement="#{placement}"
        #{format}
        #{source}
        #{disabled_str}
        data-original-title="#{display_name}">#{display_value.h}</a>
      EOS
    end

    # config_item(name: "Category name", obj: category, field: "name")
    # Enum.options
    def config_item opt
      field         = opt.fetch :field
      postfix       = opt.fetch :postfix, ""
      disabled      = opt.fetch :disabled, false
      display_name  = opt.fetch :name, field.capitalize
      value         = opt.fetch(:obj).send(field).to_s
      a             = disabled ? value : construct_xeditable(opt)
      @_out_buf << <<-EOS
      <div class="config-item">
        <label>#{display_name}</label>
        <div>#{a} <span>#{postfix}</span></div>
      </div>
      EOS
    end

    def static_item opt
      name    = opt.fetch :name
      value   = opt.fetch :value
      @_out_buf << <<-EOS
      <div class="config-item">
        <label>#{name}</label>
        <div>#{value}</div>
      </div>
      EOS
    end

    def embedded_form opt, &block
      path = opt.fetch :path
      klass = opt.fetch :klass, ""
      submit = opt.fetch :submit, "Create"
      cancel = opt.fetch :cancel, "Cancel"
      method = opt.fetch :method, "post"
      show = opt.fetch :show, false
      klass += " show" if show
      form = <<-EOS
      <form action="#{path}" method="#{method}" accept-charset="utf-8"
        enctype="multipart/form-data"
        class="embedded #{klass}">
        <div class="content">
          #{capture(&block)}
        </div>
        <div class="btns">
          <span class="btn submit">#{submit}</span>
          <span class="btn cancel">#{cancel}</span>
        </div>
        <input type="submit" class="hidden" hidefocus="true" />
      </form>
      EOS
      @_out_buf << form.force_encoding("utf-8")
    end

    def days_ago date
      days = (DateTime.now - date).to_i
      days == 0 ? "Today" : "#{days} days ago"
    end

    def days_left date
      days = (date - Date.today).floor + 1
      days <= 0 ? "Now" : "#{days}d left"
    end

    def puts_image_with_url url
      html = %Q{<img src="#{url}">}
      @_out_buf << html
    end

    def image_url_of obj, index=0
      obj["product_images"][index]["url"] rescue ""
    end

    def image_of obj, index=0
      url = image_url_of obj, index
      if url && url != ""
        puts_image_with_url url
      end
    end

    def th_image_of obj, index=0
      url = obj["product_images"][index]["th_url"] rescue ""
      if url && url != ""
        puts_image_with_url url
      end
    end

    def other_images_of product
      other_images = product["product_images"][1..-1]
      other_images.each { |image|
        @_out_buf << <<-EOS
          <a class="small-photo" href="#{image["url"]}" target="_blank">
            <img src="#{image["th_url"]}">
          </a>
        EOS
      }
    end

    def render_longtext text
      text.lines { |line|
        @_out_buf << "<p>#{line}</p>"
      }
    end

    def query_hash
      @query_hash ||= Rack::Utils.parse_nested_query request.query_string
    end

    def referer_query_hash
      @referer_query_hash ||= Rack::Utils.parse_nested_query(
        URI.parse(request.referer).query
      )
    end

    def form_url *args
      '/' + args.map { |k| k.url }.join('/')
    end

    def cached_erb path, opt
      # todo:
      # cache according to md5 check sum of opt.inspect
      erb path, opt
    end

    def current_host
      "#{request.scheme}://#{request.host_with_port}"
    end

    def current_full_path path=request.path
      "#{current_host}#{path}"
    end

    # google analytics
    # remember to add line below in initPage method
    # ga('send', 'pageview', location.pathname);
    def ga_js track_id, site_host
      @_out_buf << <<-EOS
      <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', '#{track_id}', '#{site_host}');
      </script>
      EOS
    end

    # respond.js and html5 shim for ie8
    def ie_scripts
      @_out_buf << <<-EOS
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
      EOS
    end

    def read_csv path
      rows = []
      CSV.foreach(path, headers: true, encoding: "windows-1251:utf-8") do |row|
        rows << row.each_with_object({}) { |(k, v), h|
          k and h[k.strip] = v
        }
      end
      rows
    end

    def recaptcha_correct? info
      return true if ENV["RACK_ENV"] == "development"

      require 'net/http'
      verify = 'http://api-verify.recaptcha.net'
      private_key = "6Lfr7e8SAAAAAEQy3t_hV8VVWYta7imlZmwWjwsu"
      recaptcha = Net::HTTP.post_form URI.parse("#{verify}/verify"), {
        :privatekey => private_key,
        :remoteip   => request.ip,
        :challenge  => info["recaptcha_challenge_field"],
        :response   => info["recaptcha_response_field"],
      }
      answer, _ = recaptcha.body.split.map { |s| s.chomp }
      unless answer == 'true'
        return false
      else
        return true
      end
    end

    def index_as page_title
      @index_as = page_title
    end

    def fb_page_widget(url, width, height)
      url = CGI.escape(url)
      <<-HTML
      <iframe src="//www.facebook.com/plugins/likebox.php?href=#{url}&amp;width=#{width}px&amp;height=#{height}px&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=241412406007213"
        scrolling="no"
        frameborder="0"
        style="border:none; overflow:hidden; width:#{width}px; height:#{height}px;"
        allowTransparency="true">
      </iframe>
      HTML
    end
  end

end
