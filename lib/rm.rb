require 'redis'
require 'yaml'

$models = YAML.load(File.read('config/rm.yml'))
linkages = $models['linkages']
$models = $models['models']
linkages.each { |left, type, right|
  r_type = { '1-n' => 'n-1', 'n-n' => 'n-n', '1-1' => '1-1' }[type]
  raise "Unrecognized type #{type}" if r_type.nil?
  l, r = $models.values_at(left, right)
  raise "Undefined model: #{left}" if l.nil?
  raise "Undefined model: #{right}" if r.nil?
  l['linkages'] ||= {}
  r['linkages'] ||= {}
  l['linkages'][right] = type
  r['linkages'][left] = r_type
} if linkages

class RM
  def self.list
    $models
  end

  def self.[](type)
    @rm ||= {}
    @rm[type] ||= new(type: type)
  end

  private def initialize(type: type)
    @type = type.to_s
    @definition = self.class.list[@type]
    @definition['linkages'] ||= {}
    raise "Unknown type #{@type}" if @definition.nil?
  end

  private def redis
    @redis ||= Redis.new
  end

  def create(options={})
    options[:uuid] ||= SecureRandom.uuid
    options[:created_at] ||= Time.now.to_i
    options[:_type] = @type
    @definition.fetch('required', []).each { |key|
      raise "#{key} is required but missing." unless options.key?(key.to_sym)
    }
    update(options)
    options[:uuid]
  end

  def update(uuid:, **options)
    current = get(uuid)

    fire_hook(type: :before_save, current: current, updates: options)

    # handle auto links
    @definition['linkages'].each { |target_type, type|
      if type.end_with?('1') # auto link
        target_uuid = options[:"#{target_type}_uuid"]
        link(uuid: uuid, target_uuid: target_uuid) if target_uuid
      end
    }
    # save to redis
    options[:updated_at] ||= Time.now.to_i
    redis.mapped_hmset("resource:#{uuid}", options.merge(uuid: uuid))
    redis.sadd("index:#{@type}", uuid)

    precache_filters
    fire_hook(type: :after_save, current: current, updates: options)

    true
  end

  private def fire_hook(type:, **options)
    @hooks ||= {}
    @hooks.fetch(type, []).each { |callback|
      callback.call(options)
    }
  end

  private def precache_filters
    if @definition['filters']
      resources = all
      @definition['filters'].each { |key|
        index_key = "index:#{@type}=#{key}"
        tags = {}
        resources.each { |r|
          tags[r[key]] ||= []
          tags[r[key]] << r['uuid']
        }
        redis.del(index_key)
        tags.each { |v, uuids|
          redis.sadd(index_key, v)
          redis.del("#{index_key}:#{v}")
          uuids.each { |tag_uuid|
            redis.sadd("#{index_key}:#{v}", tag_uuid)
          }
        }
      }
    end
  end

  def link(uuid:, target_uuid:, unlink: false)
    return if uuid.nil? || target_uuid.nil?
    target_type = get_resource_type(uuid: target_uuid)
    type = @definition['linkages'][target_type]
    raise "#{@type} has no relationship to #{target_type}" unless type
    unless unlink # auto unlink
      if type[-1] == '1'
        current_target = redis.get("index:#{@type}-#{target_type}:#{uuid}")
        unlink(uuid: uuid, target_uuid: current_target) if current_target
      elsif type[0] == '1'
        current_target = redis.get("index:#{target_type}-#{@type}:#{target_uuid}")
        unlink(uuid: uuid, target_uuid: current_target) if current_target
      end
    end
    methods = unlink ? { 'n' => :srem, '1' => :del } : { 'n' => :sadd, '1' => :set }
    redis.send(methods[type[0]], "index:#{target_type}-#{@type}:#{target_uuid}", uuid)
    redis.send(methods[type[-1]], "index:#{@type}-#{target_type}:#{uuid}", target_uuid)
  end

  def unlink(uuid:, target_uuid:)
    link(uuid: uuid, target_uuid: target_uuid, unlink: true)
  end

  def destroy(uuid:)
    hash = get(uuid)
    fire_hook(type: :before_destroy, model: hash)
    @definition['linkages'].each { |target_type, type|
      if type.end_with?('n')
        redis.smembers("index:#{@type}-#{target_type}:#{uuid}").each { |target_uuid|
          unlink(uuid: uuid, target_uuid: target_uuid)
        }
        redis.del("index:#{@type}-#{target_type}:#{uuid}")
      else # end with 1
        unlink(uuid: uuid, target_uuid: hash["#{target_type}_uuid"])
      end
    }
    redis.srem("index:#{@type}", uuid)
    redis.del("resource:#{uuid}")
    fire_hook(type: :after_destroy, model: hash)
    true
  end

  def all(options={})
    uuids = options.map { |k, v|
      if %w[n-1 n-n].include?(@definition['linkages'][k.to_s]) # linkages
        redis.smembers("index:#{k}-#{@type}:#{v}")
      elsif @definition['filters'].include?(k.to_s) # filters
        index_key = "index:#{@type}=#{k}"
        redis.sismember(index_key, v) ? redis.smembers("#{index_key}:#{v}") : []
      else
        raise "#{k} cannot be a filter to list #{@type}"
      end
    }.compact
    uuids = [redis.smembers("index:#{@type}")] if uuids.empty?
    op = options[:operation] == :or ? :or : :and
    uuids = op == :or ? uuids.reduce(:|) : uuids.reduce(:&)
    uuids.map { |uuid| get(uuid) }
  end

  def get(uuid)
    hash = redis.hgetall("resource:#{uuid}")
    @definition['linkages'].each { |target_type, type|
      if type.end_with?('1')
        target_id = redis.get("index:#{@type}-#{target_type}:#{uuid}")
        hash["#{target_type}_uuid"] = target_id
      end
    }
    hash
  end

  def hook(type, &callback)
    @hooks ||= {}
    unless %w[before_save after_save before_destroy after_destroy].include?(type.to_s)
      raise "Unsupported #{type}"
    end
    @hooks[type.to_sym] ||= []
    @hooks[type.to_sym] << callback
  end

  private def get_resource_type(uuid:)
    redis.hget("resource:#{uuid}", "_type")
  end
end
