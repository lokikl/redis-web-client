$(function() {

  var $body = $('body');

  var pointCache = {};
  var selectedTrade = null;

  Highcharts.setOptions({
    global: { timezoneOffset: -8 * 60 }
  });

  // store all points into pointCache
  function updatePointCache(points) {
    points.forEach(function(p) {
      var yaxis = p.series.yAxis.axisTitle.textStr;
      if (!(yaxis in pointCache)) { pointCache[yaxis] = {}; }
      pointCache[yaxis][p.series.name] = p;
    });
  }

  function descriptionForTrade(trade) {
    var s = '<br/>From <b>' + Highcharts.dateFormat('%Y-%m-%d %H:%M', trade.from) + '</b>';
    s += ' to <b>' + Highcharts.dateFormat('%Y-%m-%d %H:%M', trade.to) + '</b>';
    s += ', earned: <b>' + trade.profit + ' (' + trade.order_type + ')</b>';
    return s;
  }

  var chartTooltipFormatter = function() {
    var s = '';
    updatePointCache(this.points);
    var cx = 0;
    var cy = null;
    var firstDate = true;
    var tradeIsValid = false;
    var ts = this.points[0].x; // current timestamp

    for (var yaxis in pointCache) {
      for (var serie in pointCache[yaxis]) {
        var p = pointCache[yaxis][serie];
        var pt = p.point;
        // update points in pointCache and display
        if (pt.x !== ts) {
          for (var pi in p.series.points) {
            var pp = p.series.points[pi];
            if (pp.x > ts) {
              var i = p.series.points.indexOf(pp) - 1;
              pt = p.series.points[i];
              break;
            }
          }
        }
        if (cx !== p.series.data.length) {
          if (firstDate === true) { firstDate = false } else { s += '<br style="margin-bottom: 30px"/>'; }
          s += '<b>' + Highcharts.dateFormat('%Y-%m-%d %H:%M', pt.x) + '</b>';
          cx = p.series.data.length;
        }
        var ptitle = p.series.name.replace(yaxis, '');
        if (yaxis != cy) {
          s += '<br/><span style="display: inline-block; width:30px; visibility: hidden"> - </span>';
          s += yaxis + ': <b>';
          if (pt.high) { // ohlc
            s += 'O ' + pt.open + ' H ' + pt.high + ' L ' + pt.low;
            s += ' <span style="color:green; font-weight: bold;">C ' + pt.close + '</span>';
          } else {
            s += ptitle + ' ' + Number(pt.y.toFixed(3));
          }
          s += '</b>';
          cy = yaxis;
        } else {
          s += ', <b>' + ptitle + ' ' + Number(pt.y.toFixed(3)) + '</b>';
        }
        if (selectedTrade && p.x >= selectedTrade.from && p.x <= selectedTrade.to) {
          tradeIsValid = true;
        }
      }
    }
    if (!tradeIsValid) { selectedTrade = null; }
    if (selectedTrade) s += descriptionForTrade(selectedTrade);
    return s;
  };

  var chartTooltipPositioner = function(labelWidth, labelHeight, point) {
    var x = $body.width() - labelWidth - 61;
    var y = $body.scrollTop();
    if (y > 80) { y -= 80; }
    return { x: x, y: y }; // fixed Y position
  };

  var chartTooltipPositionerV2 = function(labelWidth, labelHeight, point) {
    var x = point.plotX - chart.plotLeft;
    var y = $body.scrollTop();
    if (y > 80) { y -= 80; }
    return { x: x, y: y }; // fixed Y position
  };

  // function called when mouse enter the band
  var updateBandInfo = function() {
    selectedTrade = {
      from: this.options.from,
      to: this.options.to,
      profit: this.options.profit,
      order_type: this.options.order_type
    };
  };

  var chart = null;
  var maxZoom = 70000000;
  var allTimestamps = [];

  window.initGraph = function(id, graphOptions) {
    var $wrap = $('#' + id);
    if ($wrap.length === 0) return;
    $wrap.data('graph-options', graphOptions);
    window.initGraphReal($wrap);
  }

  window.initGraphReal = function($wrap) {
    var graphOptions = $wrap.data('graph-options');
    // graphOptions.xAxis.plotBands.forEach(function(band) {
    //   if (band.profit) {
    //     band.events = {
    //       "mouseenter": updateBandInfo,
    //       "mouseover": updateBandInfo,
    //     }
    //   }
    // });
    // pointCache = {};
    // graphOptions.tooltip.positioner = chartTooltipPositioner;
    // debugger;
    graphOptions.tooltip.positioner = chartTooltipPositionerV2;
    graphOptions.tooltip.formatter = function() {
      var date = Highcharts.dateFormat('%Y-%m-%d %H:%M', this.x);
      var s = '<b>'+ date +'</b>';

      $.each(this.points, function(i, point) {
        var pt = point.point;
        if (pt.high) { // ohlc
          s += '<br/>K 5m: O ' + pt.open + ' H ' + pt.high + ' L ' + pt.low;
          s += ' <span style="color:green; font-weight: bold;">C ' + pt.close + '</span>';
          var surplus = point.series.tooltipOptions.surplus;
          for (var code in surplus) {
            var data = surplus[code];
            if (data[point.x]) {
              s += '<br/>' + code + ': ' + data[point.x];
            }
          }
        } else {
          s += '<br/><span style="color:'+ point.series.color +'">\u25CF</span>: ' + point.series.name + ': ' + point.y.toFixed(2);
        }
      });
      return s;
    }
    // graphOptions.tooltip.formatter = chartTooltipFormatter;

    chart = $('.graph-wrap', $wrap).highcharts('StockChart', graphOptions).highcharts();
    chart.zoomOut();
    // allTimestamps = _.last(chart.series).data.map(function(d) {
    //   return d.x;
    // });
    // console.log(allTimestamps.length);
  };

  function zoomChart(scale) {
    var axis = chart.xAxis[0];
    var e = axis.getExtremes();
    var half = (e.max - e.min) / 2;
    var center = e.min + half;
    var newhalf = half * scale;
    if (newhalf < maxZoom) newhalf = maxZoom;
    var newmin = center - newhalf;
    var newmax = center + newhalf;
    // console.log('Zooming to ' + newmin + ', ' + newmax);
    axis.setExtremes(newmin, newmax, true, true);
  }

  // startOnTick and endOnTick must be turned off
  function panChart(offset) {
    var axis = chart.xAxis[0];
    var e = axis.getExtremes();
    var si = _.indexOf(allTimestamps, _.find(allTimestamps, function(i) { return i >= e.min; }));
    var ei = _.indexOf(allTimestamps, _.find(allTimestamps, function(i) { return i >= e.max; }));
    var count = ei - si;
    offset = Math.round(offset * count / 2);
    var newsi = si + offset;
    if (newsi < 0) { newsi = 0 }
    if (newsi > allTimestamps.length - count) { newsi = allTimestamps.length - count; }
    var newei = newsi + count;
    axis.setExtremes(allTimestamps[newsi], allTimestamps[newei], true, true);
  }

  function toggleBands(type) {
    var first = chart.xAxis[0].plotLinesAndBands.filter(function(b) {
      return b.options.order_type === type;
    })[0];
    if (first === null) return;
    var visiable = !first.hidden;
    chart.xAxis[0].plotLinesAndBands.forEach(function(b) {
      if (b.options.order_type === type && 'svgElem' in b) {
        if (visiable) {
          b.hidden = true;
          b.svgElem.hide();
        } else {
          b.hidden = false;
          b.svgElem.show();
        }
      }
    });
  }

  $(window).on('statechangecomplete', function() {
    $('.graph-container[data-graph-options]').each(function() {
      var $this = $(this);
      if ($this.data('graph-initialized')) return true;
      window.initGraphReal($this);
      $this.data('graph-initialized', true);
    });
  });

  $body.on('click', 'a.graph-toggle-long', function() {
    toggleBands('long');
  });

  $body.on('click', 'a.graph-toggle-short', function() {
    toggleBands('short');
  });

  $body.on('click', 'div.graph-control > a.zoom-out', function() {
    zoomChart(2);
  });

  $body.on('click', 'div.graph-control > a.zoom-all', function() {
    chart.zoomOut();
  });

  $body.on('click', 'div.graph-control > a.zoom-in', function() {
    zoomChart(0.5);
  });

  $body.on('click', 'div.graph-control > a.pan-left', function() {
    panChart(-1);
  });

  $body.on('click', 'div.graph-control > a.pan-right', function() {
    panChart(1);
  });

});
