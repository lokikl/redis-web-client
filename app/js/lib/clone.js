$(function() {

  // data-clone-from=".indicator-$value"
  // data-clone-to="#params"
  $('body').on('change', '[data-clone-from]', function() {
    var $this = $(this);
    var from = $this.data('clone-from');
    var to = $this.data('clone-to');
    if (from && to) {
      var value = $this.val();
      if (value === '') {
        $(to).html('');
      } else {
        var $node = $(from.replace('$value', value)).clone();
        $('[data-name]', $node).each(function() {
          var $input = $(this);
          $input.prop('name', $input.data('name'));
        });
        $node.removeClass('hidden');
        $(to).html('').append($node);
      }
    }
  });

  // $(window).on('statechangecomplete', function() {
  //   $('[data-clone-from]').trigger('change');
  // });


});
