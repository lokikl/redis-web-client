/**
 * $.parseParams - parse query string paramaters into an object.
 */
(function($) {
  var re = /([^&=]+)=?([^&]*)/g;
  var decodeRE = /\+/g;  // Regex for replacing addition symbol with a space
  var decode = function (str) {return decodeURIComponent( str.replace(decodeRE, " ") );};
  $.parseParams = function(query) {
    var params = {}, e;
    while ( e = re.exec(query) ) {
      var k = decode( e[1] ), v = decode( e[2] );
      if (k.substring(k.length - 2) === '[]') {
        k = k.substring(0, k.length - 2);
        (params[k] || (params[k] = [])).push(v);
      }
      else params[k] = v;
    }
    return params;
  };
})(jQuery);

$(function() {

  function hide(query) {
    $(query)
      .addClass('hidden')
      .find('.disable-when-hidden:hidden').attr('disabled', 'disabled');
  }

  function show(query, interval) {
    if (!query) return;
    if (interval === null) {
      interval = 600;
    }
    var $ele = $(query);
    if ($ele.hasClass('hidden')) {
      $ele
        .removeClass('hidden')
        .css('opacity', 0)
        .animate({'opacity': 1}, 0);
    }
    $ele.find('.disable-when-hidden:visible').removeAttr('disabled');
  }

  $('body').on('click.hideshow', '[data-hide]', function() {
    var $this = $(this);
    var q = $this.data();

    if ($this.hasClass('trigger-scrollreveal')) {
      setTimeout(function() {
        window.reveal.init();
      }, q.interval + 100);
    }

    hide(q.hide);
    show(q.show, q.interval);

    $this.parents('.hideshow-parent')
      .find('.hideshow-current').removeClass('hideshow-current');

    $this.addClass('hideshow-current');

    // update the 'search' part of current href
    var qstring = $this.data('update-querystring');
    if (qstring) {
      var href = location.href;
      var query = $.parseParams(location.search.substr(1))
      var to = qstring.split('=');
      query[to[0]] = to[1];
      var output = $.param(query);
      history.pushState(null, '', location.pathname + '?' + output);
    }
  });

  $('body').on('click.hideshow', '[data-toggle],[data-hideshow-toggle]', function() {
    var q = $(this).data();
    $(q.hideshowToggle || q.toggle).each(function() {
      var $ele = $(this);
      if ($ele.hasClass('hidden')) {
        show($ele, q.interval);
      } else {
        hide($ele, q.interval);
      }
    });
  });

  $('body').on('change.hideshow', 'select[data-hideshow]', function() {
    var q = $(this).data();
    var interval = q.interval || 0;
    hide(q.hideshow, interval);
    $(this).children('option:selected').each(function() {
      show($(this).data('show'), interval);
    });
  });

  $(window).on('statechangecomplete', function() {
    $('.hideshow-current').each(function() {
      var $this = $(this);
      if ($this.is('option')) {
        $this.parent().trigger('change.hideshow');
      } else {
        $this.trigger('click.hideshow');
      }
    });
  });

});
