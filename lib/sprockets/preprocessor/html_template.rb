class HTMLTemplate < Tilt::Template
  def self.default_mime_type
    "text/html"
  end

  def prepare
  end

  def evaluate scope, locals
    name = scope.pathname.basename('.js.html').to_s
    "Templates['#{name}'] = #{data.inspect};"
  end
end
