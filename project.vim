nnoremap sa :exec 'CtrlP app/controllers'<cr>
nnoremap sc :exec 'CtrlP app/css'<cr>
nnoremap sh :exec 'CtrlP app/views'<cr>
nnoremap sj :exec 'CtrlP app/js'<cr>
nnoremap sl :exec 'CtrlP app/lib/'<cr>
nnoremap sw :exec 'CtrlP app/workers'<cr>
nnoremap ss :exec 'CtrlP spec'<cr>

" new approach: restart loper on a tmux session, find loper window
nnoremap <leader>r :call RestartAll()<cr>

function! RestartAll()
  call FocusMainTmuxSession()
  let p = getcwd()
  let cmd = " C-c ' cd " . p . " ; foreman start -p 8888' Enter"
  call TmuxSendKey("main", "webapp-base", cmd)
endfunction

" ============================================================================

autocmd BufEnter *.js nnoremap rjs :RequreJS 
command! -complete=custom,JSLibs -nargs=1 RequreJS exec "normal i//= require vendor/<args>"
function! JSLibs(A,L,P)
  return system("ls -1 app/js/vendor | sed -e 's/\.js//'")
endfunction

autocmd BufEnter *.scss nnoremap rcss :RequreCSS 
command! -complete=custom,CSSLibs -nargs=1 RequreCSS exec "normal i//= require vendor/<args>"
function! CSSLibs(A,L,P)
  return system("ls -1 app/css/vendor | sed -e 's/\.css//'")
endfunction

function! Rspec_current()
  let path = expand("%:p")
  exe "silent ! tmux send-keys -t rspec " . path . " Enter &"
endfunction
nnoremap <F2> :call Rspec_current()<cr>
