$load_app = true
$compile_asset = true
require File.dirname(__FILE__) + '/config/boot.rb'

unless $production
  map '/assets' do
    require './lib/sprockets/bootstrap'
    require 'sprockets-sass'
    Sprockets::Sass.options[:debug_info] = true
    Sprockets::Sass.options[:line_comments] = true
    Sprockets::Sass.options[:sourcemap] = false
    sprockets = setup_sprockets

    run sprockets
  end
end

map '/' do
  Encoding.default_external = Encoding::UTF_8
  Encoding.default_internal = Encoding::UTF_8

  session_conf = {
    secret: "SecrEtIsASeCreT",
    expire_after: 1.week,
  }

  use Rack::Session::Cookie, session_conf

  run WebApp
end

map '/sidekiq' do
  require 'sidekiq'
  require 'sidekiq/web'

  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    username == 'admin' && password == 'admin'
  end

  run Sidekiq::Web
end
