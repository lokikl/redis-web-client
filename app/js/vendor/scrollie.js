$(function() {

  var $window = $(window);
  var $body = $('body');
  var timelines = {};

  $window.scroll(function() {
    var top = $window.scrollTop();

    // duration = screen height + area height
    var screenHeight = $window.innerHeight();

    for (var name in timelines) {
      var tl = timelines[name];
      var domTop = tl.dom.offset().top;
      var domHeight = tl.dom.height();
      scrollTween(top, domTop - screenHeight, domTop + screenHeight + domHeight, tl.timeline);
    }
  });

  function objString2Obj(string) {
    if (!string) return false;
    var properties = string.split(',');
    var obj = {};
    properties.forEach(function(property) {
        var tup = property.split(':');
        var key = $.trim(tup[0]);
        obj[key] = $.trim(tup[1]);
    });
    return obj;
  }

  function addKeyframe($obj, tl) {
    var data = $obj.data();
    if (data.scrollieInitial) {
      var initial = objString2Obj(data.scrollieInitial);
      var end = objString2Obj(data.scrollieEnd);
      var duration = data.scrollieDuration || 1;
      var start = data.scrollieStart || 0;
      if (initial && end) {
        tl.fromTo($obj, duration, initial, end, start);
      } else if (initial) {
        tl.from($obj, duration, initial, end, start);
      } else {
        tl.to($obj, duration, initial, end, start);
      }
    }
  }

  function scrollTween(top, startPoint, endPoint, tweenName) {
    var progressNumber;
    progressNumber = (1 / (endPoint - startPoint)) * (top - startPoint);
    progressNumber = Math.max(0, progressNumber);
    progressNumber = Math.min(1, progressNumber);
    tweenName.progress(progressNumber);
  }

  $window.on('statechangecomplete', function() {
    timelines = {};
    $('[data-scrollie-timeline]').each(function() {
      var $timeline = $(this);
      var name = $timeline.data('scrollie-timeline');
      timelines[name] = {
        dom: $timeline,
        timeline: new TimelineMax({paused:true})
      }
      var tl = timelines[name].timeline;
      addKeyframe($timeline, tl);
      $('[data-scrollie-initial]', $timeline).each(function() {
        addKeyframe($(this), tl);
      });
      var pad = 1 - tl.totalDuration();
      tl.to($body, pad, { autoAlpha: "1" });
    });
    $window.scroll();
  });

});
