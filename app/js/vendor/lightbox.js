$(function() {

  var vpSize = '0.9'; // image width/height is at most 90% of the viewport
  var $lightbox = $('#lightbox');
  var $window = $(window);
  var $body = $('body');
  var $currentNode = null;

  function setupDOM() {
    if (!!$lightbox.length) return;
    var html = '<div id="lightbox"><img src=""/><i class="throbber icon-spinner icon-spin"></i></div>';
    $body.append(html);
    $lightbox = $('#lightbox').hide(0);
  }

  $('body').on('click', '[data-lightbox]', function() {
    setupDOM();
    $currentNode = $(this);
    var $this = $(this);
    var imgUrl = $this.data('lightbox');
    var image = new Image();
    var $throbber = $('i.throbber', $lightbox).show(0);
    var $img = $('img', $lightbox).hide(0);

    $lightbox.fadeIn(400);

    $(image).load(function() {
      var sWidth = $window.width() * vpSize;
      var sHeight = $window.height() * vpSize;
      var r = this.width / this.height;
      if (this.width > sWidth) {
        this.height = sWidth / r;
        this.width = sWidth;
      }
      if (this.height > sHeight) {
        this.width = sHeight * r;
        this.height = sHeight;
      }
      var topPad = ((1 - vpSize) * sHeight + (sHeight - this.height)) / 2;

      $throbber.hide(0);
      $img
        .prop('src', imgUrl)
        .css({
          width: this.width,
          height: this.height,
          marginTop: topPad
        })
        .fadeIn(400);
    })[0].src = imgUrl; // end of image loading
  });

  $('body').on('click', '#lightbox', function() {
    $(this).fadeOut(400);
    $currentNode = null;
  });

  $('body').on('keydown', function(e) {
    if (!$currentNode) return;
    if (e.which === 37) { // left
      var $next = $currentNode.prev();
      $next.trigger('click');
    } else if (e.which === 39) { // right
      var $next = $currentNode.next();
      $next.trigger('click');
    }
  });

});
