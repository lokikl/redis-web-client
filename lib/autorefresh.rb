class AutoRefresh
  def initialize(app)
    @app = app
    @tag = '<script src="http://127.0.0.1:8000/push.js" type="text/javascript" charset="utf-8"></script>'
  end

  def call(env)
    status, headers, body = @app.call(env)

    if headers && headers["Content-Type"] && headers["Content-Type"]["html"]
      # valid = is html that can add push.js in
      valid = headers["Content-Disposition"] != "attachment"
      valid &&= !body.nil?
      valid &&= !body.is_a?(Rack::File)
      valid &&= !body[0].nil?

      if valid
        body[0] = body[0].sub "</body>", @tag + "</body>"

        size = 0
        body[0].lines.each { |line|
          size += line.bytesize
        }

        headers['Content-Length'] = size.to_s
      end
    end

    [status, headers, body]
  end
end
